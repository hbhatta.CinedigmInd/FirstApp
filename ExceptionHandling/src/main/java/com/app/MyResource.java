package com.app;

import com.app.exception.CustomerDataNotFoundException;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource/")
public class MyResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }
    
    @GET
	@Path("/checkProfile/{id}")
	public Response getAdminDetails(@PathParam("id") String id) throws CustomerDataNotFoundException {
		
		String msg;
		try {
			msg = RESTService.checkCustomerStatus(id);
		} catch (CustomerDataNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CustomerDataNotFoundException("Internal server Error");
		}
 
		return Response.status(200).entity(msg).build(); 
	} 	
}
