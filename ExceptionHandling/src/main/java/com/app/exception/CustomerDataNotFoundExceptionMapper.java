package com.app.exception;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
@Provider
public class CustomerDataNotFoundExceptionMapper implements ExceptionMapper<CustomerDataNotFoundException> {

	@Override
	public Response toResponse(CustomerDataNotFoundException exception) {
		
		return Response.status(Status.NOT_FOUND)
				.entity(new ErrorProps("404", exception.getMessage()))
				.build();
	}

}
