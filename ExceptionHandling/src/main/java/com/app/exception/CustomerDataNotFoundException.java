package com.app.exception;

public class CustomerDataNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public CustomerDataNotFoundException(String exceptionMsg)
	{
		super(exceptionMsg);
	}
}
