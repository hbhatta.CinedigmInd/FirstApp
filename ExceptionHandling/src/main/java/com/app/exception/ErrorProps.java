package com.app.exception;

import java.util.Objects;

public class ErrorProps {
	private String status;
	private String errorMessage;
	public ErrorProps() {
		
	}
	public ErrorProps(String status, String errorMessage) {
		super();
		this.status = status;
		this.errorMessage = errorMessage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	@Override
	public int hashCode() {
		return Objects.hash(errorMessage, status);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ErrorProps other = (ErrorProps) obj;
		return Objects.equals(errorMessage, other.errorMessage) && Objects.equals(status, other.status);
	}

	
}
