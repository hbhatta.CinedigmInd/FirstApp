package com.app;

import com.app.exception.CustomerDataNotFoundException;

public class RESTService {
	
	public static CallDB cdb = new CallDB();
	
	public static String checkCustomerStatus(String custId) throws CustomerDataNotFoundException{
		
		MyData da = cdb.getStatus(custId);
		if(da.getStatus() == null)
		{
			throw new CustomerDataNotFoundException("Customer status not found with id "+custId);
		}
		
		return da.getStatus().trim();			
	}
}
